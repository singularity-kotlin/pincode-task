package kz.jusan.pincodeui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import kz.jusan.pincodeui.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater) // custom binding
        setContentView(binding.root)

        val resultText = findViewById<TextView>(R.id.result)

        binding.button1.setOnClickListener { button ->
            resultText.text = resultText.text.toString() + "1"
        }

        binding.button2.setOnClickListener { button ->
            resultText.text = resultText.text.toString() + "2"
        }

        binding.button3.setOnClickListener { button ->
            resultText.text = resultText.text.toString() + "3"
        }

        binding.button4.setOnClickListener { button ->
            resultText.text = resultText.text.toString() + "4"
        }

        binding.button5.setOnClickListener { button ->
            resultText.text = resultText.text.toString() + "5"
        }

        binding.button6.setOnClickListener { button ->
            resultText.text = resultText.text.toString() + "6"
        }

        binding.button7.setOnClickListener { button ->
            resultText.text = resultText.text.toString() + "7"
        }

        binding.button8.setOnClickListener { button ->
            resultText.text = resultText.text.toString() + "8"
        }

        binding.button9.setOnClickListener { button ->
            resultText.text = resultText.text.toString() + "9"
        }

        binding.buttonZero.setOnClickListener { button ->
            resultText.text = resultText.text.toString() + "0"
        }

        binding.buttonOK.setOnClickListener {
            if( resultText.text.toString().equals("1567")) {
                Toast.makeText(this, "CORRECT PIN", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "WRONG PIN", Toast.LENGTH_SHORT).show()
            }
        }

        binding.buttonClear.setOnClickListener {
            resultText.text = ""
        }

    }
}